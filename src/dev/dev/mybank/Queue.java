package dev.mybank;

import dev.mybank.clients.Client;
import dev.mybank.clients.types.Type;

import java.util.ArrayList;
import java.util.List;


public class Queue {

    private List<Client> store;
    private static final int maxDeposits = 1000000;
    private static final int minCredits = 50000;
    private int totalDeposits = 0;
    private int totalCredits = 0;


    public Queue() {
        store = new ArrayList<>();
    }

    public synchronized boolean add(Client element) {

        try {
            if (totalDeposits < maxDeposits & totalCredits < minCredits) {
                notifyAll();
                store.add(element);
                String info = String.format("Client added", store.size(), element.getType(), element.getSize(), Thread.currentThread().getName());
                System.out.println(info);


            } else {
                System.out.println(store.size() + "out of limit " + Thread.currentThread().getName());
                wait();
                return false;
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return true;
    }

    public synchronized Client get(Type CREDIT) {

        try {
            if (totalDeposits < maxDeposits & totalCredits < minCredits) {
                notifyAll();
                for (Client CLIENT : store) {
                    if (CLIENT.getType() == CREDIT) {

                        System.out.println(store.size()  + Thread.currentThread().getName());
                        store.remove(CLIENT);
                        return CLIENT;
                    }
                }
            }

            System.out.println("0 < There are no  clients");
            wait();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }


}
