package dev.mybank.tasks;

import dev.mybank.clients.Client;
import dev.mybank.clients.types.Size;
import dev.mybank.Queue;
import dev.mybank.clients.types.Type;

import java.util.Random;


public class ClientGenerator implements Runnable {
    private Queue queue;
    private int clientCount=10000;

    public ClientGenerator(Queue queue, int clientCount) {
        this.queue = queue;
        this.clientCount = clientCount;
    }

    @Override
    public void run() {
        int count = 0;
        while (count < clientCount) {
            Thread.currentThread().setName(" Generate client");
            count++;
            queue.add(new Client(getRandomSize(), getRandomType()));
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    private Type getRandomType() {
        Random random = new Random();
        return Type.values()[random.nextInt(Type.values().length)];
    }

    private Size getRandomSize() {
        Random random = new Random();
        return Size.values()[random.nextInt(Size.values().length)];
    }
}
