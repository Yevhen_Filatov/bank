package dev.mybank.clients.types;


public enum Size {
    SMALL(1000), MIDDLE(5000), LARGE(2000);

    Size(int value){
        this.value = value;
    }
    private int value;

    public int getValue() {
        return value;
    }
}
