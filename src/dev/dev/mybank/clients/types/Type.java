package dev.mybank.clients.types;


public enum Type {
    CREDIT, DEPOSIT
}
