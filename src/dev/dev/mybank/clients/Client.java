package dev.mybank.clients;

import dev.mybank.clients.types.Size;
import dev.mybank.clients.types.Type;


public class Client {

    private Size size;
    private Type type;


    public Client(Size size, Type type) {
        this.size = size;
        this.type = type;
    }


    public Type getType() {
        return type;
    }

    public Size getSize() {
        return size;
    }
}
