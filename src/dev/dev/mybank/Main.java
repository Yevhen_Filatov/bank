package dev.mybank;

import dev.mybank.clients.types.Type;
import dev.mybank.tasks.ClientGenerator;
import dev.mybank.tasks.Cashier;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class Main {

    public static void main(String[] args) {
        System.out.println("Available number of cores: " + Runtime.getRuntime().availableProcessors());

        Queue queue = new Queue();

        ClientGenerator clientGenerator = new ClientGenerator(queue, 10000);

        Cashier cashier1 = new Cashier(queue, Type.DEPOSIT);

        Controller controller = new Controller(queue, Type.CREDIT);

        ExecutorService service = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

        service.execute(clientGenerator);
        service.execute(cashier1);

        service.execute(controller);

        service.shutdown();


    }
}
